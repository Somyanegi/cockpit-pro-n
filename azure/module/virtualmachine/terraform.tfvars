pass       = "Admin_12345"
rg_location    = "east us"
rg_name        = "my-aks-cluster"
admin_username = "azureuser"
vnet_1 = "vnet01"
subnets = [
  "subnet01",
  "subnet02"
]
vnet_1_address_space    = ["172.1.0.0/16"]
subnet_address_prefixes = [
  "172.1.0.0/17",
  "172.1.128.0/17"
]
network_nic_name                         = "myNic"
network_ip_config                        = "my_nic_config"
private_ip_address_allocation            = "Dynamic"
public_ip_address_allocation             = "Dynamic"
public_ip_name                           = "my_public_ip"
network_security_group_name              = "myNsg"
security_rule_name                       = "SSH"
security_rule_priority                   = 1001
security_rule_direction                  = "Inbound"
security_rule_access                     = "Allow"
security_rule_protocol                   = "Tcp"
security_rule_source_port_range          = "*"
security_rule_destination_port_range     = "22"
security_rule_source_address_prefix      = "*"
security_rule_destination_address_prefix = "*"
vm_name                                  = "myVM1"
os_disk_name                             = "myOsDisk"
os_disk_caching                          = "ReadWrite"
os_disk_storage_account_type             = "Premium_LRS"
vm_size                                  = "Standard_DS1_v2"
source_image_publisher                   = "Canonical"
source_image_offer                       = "0001-com-ubuntu-server-jammy"
source_image_sku                         = "22_04-lts-gen2"
source_image_version                     = "latest"
private_cluster_enabled                  = true
computer_name                            = "myVm"
disable_password_authentication          = false