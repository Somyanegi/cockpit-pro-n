
# Create a virtual network
resource "azurerm_virtual_network" "example-1" {
  name                = var.vnet_1
  address_space       = var.vnet_1_address_space
  location            = var.rg_location
  resource_group_name = var.rg_name
}

# Create a subnet within the virtual network
resource "azurerm_subnet" "example-1" {
  name                 = var.subnets[0]
  resource_group_name  = var.rg_name
  virtual_network_name = azurerm_virtual_network.example-1.name
  address_prefixes     = [var.subnet_address_prefixes[0]]

}

resource "azurerm_subnet" "example-2" {
  count                = var.private_cluster_enabled ? 1 : 0
  name                 = var.subnets[1]
  resource_group_name  = var.rg_name
  virtual_network_name = azurerm_virtual_network.example-1.name
  address_prefixes     = [var.subnet_address_prefixes[1]]

}
