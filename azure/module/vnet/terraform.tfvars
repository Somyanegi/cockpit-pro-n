rg_location             = "east us"
rg_name                 = "my-aks-cluster"
vnet_1                  = "vnet01"
subnets = [
  "subnet01",
  "subnet02"
]
vnet_1_address_space    = ["172.1.0.0/16"]
subnet_address_prefixes = [
  "172.1.0.0/17",
  "172.1.128.0/17"
]

private_cluster_enabled = true