output "subnet_1_id" {
  value = azurerm_subnet.example-1.id
}
output "subnet_id_2" {
  value = var.private_cluster_enabled ? azurerm_subnet.example-2[0].id :null
}
output "vnet_name" {
  value = azurerm_virtual_network.example-1.name
}
output "subnet_name" {
  value = azurerm_subnet.example-1.name
}