output "storage_account_name" {
  description = "The name of the Azure Storage Account."
  value       = azurerm_storage_account.aks_storage_account.name
}

output "storage_container_name" {
  description = "The name of the Azure Storage Container."
  value       = azurerm_storage_container.aks_storage_container.name
}

output "storage_blob_name" {
  description = "The name of the Azure Storage Blob."
  value       = azurerm_storage_blob.aks_storage_blob.name
}

output "storage_account_primary_connection_string" {
  description = "The primary connection string of the Azure Storage Account."
  value       = azurerm_storage_account.aks_storage_account.primary_connection_string
  sensitive = true
}

