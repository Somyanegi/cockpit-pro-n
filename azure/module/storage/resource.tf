resource "azurerm_storage_account" "aks_storage_account" {
  name                     = var.storage_account_name
  resource_group_name      = var.rg_name
  location                 = var.rg_location
  account_tier             = var.account_tier
  account_replication_type = var.account_replication_type
}

resource "azurerm_storage_container" "aks_storage_container" {
  name                  = var.storage_container_name
  storage_account_name  = azurerm_storage_account.aks_storage_account.name
  container_access_type = var.container_access_type
}

resource "azurerm_storage_blob" "aks_storage_blob" {
  name                   = var.storage_blob_name
  storage_account_name   = azurerm_storage_account.aks_storage_account.name
  storage_container_name = azurerm_storage_container.aks_storage_container.name
  type                   = var.blob_type 
}