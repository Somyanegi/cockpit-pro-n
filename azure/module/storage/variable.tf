variable "rg_location" {
  type        = string
   default     = "east us"
  description = "The location/region for the Azure resource group."
}

variable "rg_name" {
  type        = string
   default     = "my-aks-cluster"
  description = "The name for the Azure resource group."
}
variable "storage_account_name" {
  description = "Name of the Azure Storage Account"
  type        = string
  default     = "examplestoracc121"
}

variable "storage_container_name" {
  description = "Name of the Azure Storage Container"
  type        = string
  default     = "aks-container"
}

variable "storage_blob_name" {
  description = "Name of the Azure Storage Blob"
  type        = string
  default     = "aks_blob"
}

variable "account_tier" {
  description = "Storage Account Tier (e.g., Standard)"
  type        = string
  default     = "Standard"
}

variable "account_replication_type" {
  description = "Storage Account Replication Type (e.g., LRS)"
  type        = string
  default     = "LRS"
}

variable "container_access_type" {
  description = "Access type for the storage container (e.g., private)"
  type        = string
  default     = "private"
}

variable "blob_type" {
  description = "Type of the Azure Storage Blob (e.g., Block, Page, Append)"
  type        = string
  default     = "Block"
}
