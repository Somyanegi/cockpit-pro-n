variable "pass" {
  type        = string
  default     = "Admin_12345"
  description = "The password for the virtual machine admin user11.e"
}

variable "aks_name" {
  type        = string
  default     = "my-aks-cluster200"
  description = "The name for the Azure Kubernetes Service (AKS_) cluster."
}

variable "dns_prefix" {
  type        = string
  default     = "myakscluster"
  description = "The DNS prefix used for the AKS cluster's URLs."
}
variable "aks_version" {
  type        = string
  default     = "1.26.6"
  description = "The version of Kubernetes"
}

variable "rg_location" {
  type        = string
  default     = "canada central"
  description = "The location/region for the Azure resource group."
}

variable "rg_name" {
  type        = string
  default     = "my-aks-cluster101"
  description = "The name for the Azure resource groups."
}

variable "admin_username" {
  type        = string
  default     = "azureuser"
  description = "Admin username for the virtual machine."
}

variable "node_count" {
  type        = number
  default     = 1
  description = "The number of nodes in the AKS cluster's default node pool.2"
}


variable "vnet_1" {
  type        = string
  default     = "vnet01"
  description = "The name of the first virtual network."
}

variable "subnets" {
  type        = list(string)
  default     = ["subnet01", "subnet02"]
  description = "A list of subnet names."
}

variable "identity_type" {
  type        = string
  default     = "SystemAssigned"
  description = "The type of identity assigned to resources like AKS."
}

variable "default_node_pool_name" {
  type        = string
  default     = "default"
  description = "The name of the default node pool in AKS."
}

variable "default_node_vm_size" {
  type        = string
  default     = "Standard_DS2_v2"
  description = "The size of the virtual machines in the default node pool."
}

variable "enable_auto_scaling" {
  type        = bool
  # default     = false
  default     = true
  description = "Enable auto-scaling for the default node pool in AKS."
}

variable "min_node_count" {
  type        = number
  # type        = string
  # default     = null
  default     = 1
  description = "The minimum number of nodes in the default node pool."
}

variable "max_node_count" {
  type        = number
  # type        = string
  # default     = null
  default     = 4
  description = "The maximum number of nodes in the default node pool."
}
variable "availability_zones" {
  type    = list(string)
  description = "List of availability zones for AKS nodes"
  default = [ "1" ]
}
variable "network_plugin" {
  type        = string
  default     = "kubenet"
  description = "The network plugin used in AKS."
}

variable "dns_service_ip" {
  type        = string
  sensitive   = true
  default     = "192.168.1.1"
  description = "The DNS service IP address in AKS."
}

variable "service_cidr" {
  type        = string
  sensitive   = true
  default     = "192.168.0.0/16"
  description = "The service CIDR in AKS."
}

variable "pod_cidr" {
  type        = string
  sensitive   = true
  default     = "172.16.0.0/22"
  description = "The pod CIDR in AKS."
}
variable "default_node_pool_type" {
  type        = string
  default     = "VirtualMachineScaleSets"
  description = "The type of the default node pool in AKS."
}

variable "vnet_1_address_space" {
  type        = list(string)
  default     = ["172.1.0.0/16"]
  description = "The address space of the first virtual network."
}

variable "subnet_address_prefixes" {
  type        = list(string)
  default     = ["172.1.0.0/17", "172.1.128.0/17"]
  description = "A list of subnet address prefixes."
}

variable "network_nic_name" {
  type        = string
  default     = "myNic"
  description = "The name of the network interface."
}

variable "network_ip_config" {
  type        = string
  default     = "my_nic_config"
  description = "The name of the IP configuration for the network interface."
}

variable "private_ip_address_allocation" {
  type        = string
  default     = "Dynamic"
  description = "The allocation method for the private IP address."
}

variable "public_ip_address_allocation" {
  type        = string
  default     = "Dynamic"
  description = "The allocation method for the public IP address."
}

variable "public_ip_name" {
  type        = string
  default     = "my_public_ip"
  description = "The name of the public IP."
}

variable "network_security_group_name" {
  type        = string
  default     = "myNsg"
  description = "The name of the network security group."
}

variable "security_rule_name" {
  type        = string
  default     = "SSH"
  description = "The name of the security rule."
}

variable "security_rule_priority" {
  type        = number
  default     = 1001
  description = "The priority of the security rule."
}

variable "security_rule_direction" {
  type        = string
  default     = "Inbound"
  description = "The direction of the security rule (Inbound/Outbound)."
}

variable "security_rule_access" {
  type        = string
  default     = "Allow"
  description = "The access level of the security rule (Allow/Deny)."
}

variable "security_rule_protocol" {
  type        = string
  default     = "Tcp"
  description = "The protocol for the security rule."
}

variable "security_rule_source_port_range" {
  type        = string
  default     = "*"
  description = "The source port range for the security rule."
}

variable "security_rule_destination_port_range" {
  type        = string
  default     = "22"
  description = "The destination port range for the security rule."
}

variable "security_rule_source_address_prefix" {
  type        = string
  default     = "*"
  description = "The source address prefix for the security rule."
}

variable "security_rule_destination_address_prefix" {
  type        = string
  default     = "*"
  description = "The destination address prefix for the security rule."
}

variable "vm_name" {
  type        = string
  default     = "myVM1"
  description = "The name of the virtual machine."
}

variable "os_disk_name" {
  type        = string
  default     = "myOsDisk"
  description = "The name of the OS disk."
}

variable "os_disk_caching" {
  type        = string
  default     = "ReadWrite"
  description = "The caching option for the OS disk."
}

variable "os_disk_storage_account_type" {
  type        = string
  default     = "Premium_LRS"
  description = "The storage account type for the OS disk."
}

variable "vm_size" {
  type        = string
  default     = "Standard_DS2_V2"
  description = "The size of the virtual machine."
}

variable "source_image_publisher" {
  type        = string
  default     = "Canonical"
  description = "The publisher of the source image."
}

variable "source_image_offer" {
  type        = string
  default     = "0001-com-ubuntu-server-jammy"
  description = "The offer of the source image."
}

variable "source_image_sku" {
  type        = string
  default     = "22_04-lts-gen2"
  description = "The SKU of the source image."
}

variable "source_image_version" {
  type        = string
  default     = "latest"
  description = "The version of the source image."
}

variable "private_cluster_enabled" {
  type        = bool
  default     = true
  description = "Whether the AKS cluster is private."
}

variable "computer_name" {
  type        = string
  default     = "myVm"
  description = "Name of the computer for the VM."
}
variable "disable_password_authentication" {
  type        = bool
  default     = false
  description = "Set this to true if you want to disable password authentication."
}
//for storage accounyVg8Q~YTRkTM9VNCjH1DFyS7NtEmiK_aXCpGbdsHt if neccessary
# variable "storage_account_name" {
#   description = "Name of the Azure Storage Account"
#   type        = string
#   default     = "examplestoracc992"
# }

# variable "storage_container_name" {
#   description = "Name of the Azure Storage Container"
#   type        = string
#   default     = "aks-container"
# }

# variable "storage_blob_name" {
  
#   description = "Name of the Azure Storage Blob"
#   type        = string
#   default     = "aks_blob"
# }

# variable "account_tier" {
#   description = "Storage Account Tier (e.g., Standard)"
#   type        = string
#   default     = "Standard"
# }

# variable "account_replication_type" {
#   description = "Storage Account Replication Type (e.g., LRS)"
#   type        = string
#   default     = "LRS"
# }

# variable "container_access_type" {
#   description = "Access type for the storage container (e.g., private)"
#   type        = string
#   default     = "private"
# }

# variable "blob_type" {
#   description = "Type of the Azure Storage Blob (e.g., Block, Page, Append)"
#   type        = string
#   default     = "Block"
# }

# Delete Test 3
