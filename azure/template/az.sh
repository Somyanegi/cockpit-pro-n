#!/usr/bin/bash
set -x
apt update
az upgrade
az account set --subscription 908eb9a6-ffef-4860-9e90-8c7aae28bc71
az aks get-credentials --resource-group my-aks-cluster101 --name my-aks-cluster200
az aks command invoke --resource-group my-aks-cluster101 --name my-aks-cluster200 --command "kubectl create ns aks"
az aks command invoke --resource-group my-aks-cluster101 --name my-aks-cluster200 --command "kubectl apply -f dep.yaml -n aks" --file ./dep.yaml
az aks command invoke --resource-group my-aks-cluster101 --name my-aks-cluster200 --command "helm repo add fairwinds-stable https://charts.fairwinds.com/stable && helm repo update && helm upgrade --install goldilocks fairwinds-stable/goldilocks --namespace goldilocks --create-namespace --set vpa.enabled=true"
az aks command invoke --resource-group my-aks-cluster101 --name my-aks-cluster200 --command "kubectl label ns --all goldilocks.fairwinds.com/enabled=true"
az aks command invoke --resource-group my-aks-cluster101 --name my-aks-cluster200 --command "kubectl patch svc goldilocks-dashboard -n goldilocks -p '{\"spec\": {\"type\": \"LoadBalancer\"}}'"

# #!/usr/bin/bash
# apt update
# az account set --subscription 908eb9a6-ffef-4860-9e90-8c7aae28bc71
# az aks get-credentials --resource-group my-aks-cluster101 --name my-aks-cluster200
# kubectl create ns aks
# kubectl apply -f dep.yaml -n aks
# kubectl label ns --all goldilocks.fairwinds.com/enabled=true
# helm repo add fairwinds-stable https://charts.fairwinds.com/stable
# helm upgrade --install goldilocks fairwinds-stable/goldilocks --namespace goldilocks --create-namespace --set vpa.enabled=true
# kubectl patch svc goldilocks-dashboard -n goldilocks -p '{"spec": {"type": "LoadBalancer"}}'