output "public_gke_cluster_name" {
  value = var.cluster_type == "public" ? module.gke_cluster[0].name : null
}

output "private_gke_cluster_name" {
  value = var.cluster_type == "private" ? module.private_gke_cluster[0].name : null
}

output "public_gke_cluster_endpoint" {
  value = var.cluster_type == "public" ? module.gke_cluster[0].endpoint : null
  sensitive = true
}

output "private_gke_cluster_endpoint" {
  value = var.cluster_type == "private" ? module.private_gke_cluster[0].endpoint : null
  sensitive = true 
}

output "Cluster_kubeconfig" {
  description = "Path to the Kubeconfig file for the GKE cluster."
  value       = resource.local_sensitive_file.kubeconfig.filename
}