terraform {
 backend "gcs" {
   bucket  = "remote_state_bucket_cockpit23"
   prefix  = "terraform/state"
 }
}
