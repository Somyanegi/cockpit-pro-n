#!/usr/bin/bash
set -x
apt update

aws eks update-kubeconfig --name cockpit-pro-eks --region ap-south-1

 

helm repo add metrics-server https://kubernetes-sigs.github.io/metrics-server

helm upgrade --install metrics-server metrics-server/metrics-server

kubectl create ns eks

kubectl apply -f dep.yaml -n eks

kubectl label ns --all goldilocks.fairwinds.com/enabled=true

helm repo add fairwinds-stable https://charts.fairwinds.com/stable

helm upgrade --install goldilocks fairwinds-stable/goldilocks --namespace goldilocks --create-namespace --set vpa.enabled=true

kubectl patch svc goldilocks-dashboard -n goldilocks -p '{"spec": {"type": "LoadBalancer"}}'
