terraform {
  required_version = ">=1.0.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">=3.40.0"
    }
  }

  # Update your statefile stroing location here

  # backend "s3" {
  #   bucket = "terraform-statefile"
  #   key    = "terraform.tfstate"
  #   region = "us-east-1" Delete
  # }

   backend "s3" {
    bucket            = "demo-cockpit"
    key               = "terraform.tfstate"
    region            = "ap-south-1"
    # force_destroy     = true       # For smooth execution of terraform destroy
  }

}

provider "aws" {
  region = "ap-south-1"
}
