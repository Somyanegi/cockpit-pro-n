locals {
  config_map_aws_auth = <<CONFIGMAPAWSAUTH
apiVersion: v1
kind: ConfigMap
metadata:
  name: aws-auth
  namespace: kube-system
data:
  mapRoles: |
    - rolearn: ${aws_iam_role.eks-node.arn}
      username: system:node:{{EC2PrivateDNSName}}
      groups:
        - system:bootstrappers
        - system:nodes
  mapUsers: |
    - userarn: arn:aws:iam::400139313811:user/saumya
      username: saumya
      groups:
        - system:masters
    
CONFIGMAPAWSAUTH

#   kubeconfig = <<KUBECONFIG
# apiVersion: v1
# clusters:
# - cluster:
#     server: ${aws_eks_cluster.eks.endpoint}
#     certificate-authority-data: ${aws_eks_cluster.eks.certificate_authority.0.data}
#   name: kubernetes
# contexts:
# - context:
#     cluster: kubernetes
#     user: aws
#   name: aws
# current-context: aws
# kind: Config
# preferences: {}
# users:
# - name: aws
#   user:
#     exec:
#       apiVersion: client.authentication.k8s.io/v1beta1
#       command: aws-iam-authenticator
#       args:
#         - "token"
#         - "-i"
#         - "${var.cluster_name}"
# KUBECONFIG


  kubeconfig = <<KUBECONFIG
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: ${aws_eks_cluster.eks.certificate_authority.0.data}
    server: ${aws_eks_cluster.eks.endpoint}
  name: ${aws_eks_cluster.eks.arn}
contexts:
- context:
    cluster: ${aws_eks_cluster.eks.arn}
    user: ${aws_eks_cluster.eks.arn}
  name: ${aws_eks_cluster.eks.arn}
current-context: ${aws_eks_cluster.eks.arn}
kind: Config
preferences: {}
users:
- name: ${aws_eks_cluster.eks.arn}
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1beta1
      command: aws
      args:
        - --region
        - "${var.region}"
        - eks
        - get-token
        - --cluster-name
        - "${var.cluster_name}"
        # - --role
        # - "arn:aws:iam::$account_id:role/my-role"
      # env:
        # - name: "AWS_PROFILE"
        #   value: "aws-profile"
KUBECONFIG
}

output "config_map_aws_auth" {
  value = local.config_map_aws_auth
  sensitive = true
}

output "kubeconfig" {
  value = local.kubeconfig
  sensitive = true
}

resource "local_file" "kubeconfig" {
  depends_on = [aws_eks_cluster.eks]
  filename   = "kubeconfig"
  content    = local.kubeconfig
}
